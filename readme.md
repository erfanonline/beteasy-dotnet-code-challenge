BetEasy race feed parsing and sorting

This is a coding exercise from BetEasy. One json and one xml sample feeds are provided as well as a skeleton .net project. The output goal is to output horse names in price ascending order. The other goal is to assess the approach to solve the problem as well as having proper tests in place to have the code in production ready state.

Visual Studio 2017 Community Edition is used for this project.

How to run:
Common instructions:
Open solution in Visual Studio
In Solution Explorer, make sure for all files under FeedData, "Copy Always" is selected for "Copy to Output Directory"

Main code:
Build and run project: dontnet-code-challenge

Test
Menu > Test > Run > All Tests


using dotnet_code_challenge.Classes.Core;
using System;
using System.Collections.Generic;
using Xunit;

namespace dotnet_code_challenge.Test
{
    public class UnitTest1
    {
        #region sorting tests
        //no horse
        [Fact]
        public void ZeroContestants()
        {
            Contest contest = CreateTestContest();
            List<Contestant> expected = new List<Contestant>();
            var actual = contest.GetContestantsWithPriceAsc();
            Assert.Empty(actual);
        }

        //already sorted
        [Fact]
        public void AlreadySorted()
        {
            Contest contest = CreateTestContest();
            Contestant c0 = contest.AddContestant("1", "One", 5.7m);
            Contestant c1 = contest.AddContestant("2", "Two", 7.6m);
            Contestant c2 = contest.AddContestant("3", "Three", 8m);

            List<Contestant> expected = new List<Contestant>() { c0, c1, c2 };

            var actual = contest.GetContestantsWithPriceAsc();

            Assert.Equal(expected, actual);

        }

        //decendending order
        [Fact]
        public void PreSortedDesc()
        {
            Contest contest = CreateTestContest();
            Contestant c2 = contest.AddContestant("3", "Three", 8m);
            Contestant c1 = contest.AddContestant("2", "Two", 7.6m);
            Contestant c0 = contest.AddContestant("1", "One", 5.7m);


            List<Contestant> expected = new List<Contestant>() { c0, c1, c2 };

            var actual = contest.GetContestantsWithPriceAsc();

            Assert.Equal(expected, actual);

        }

        //all same price
        [Fact]
        public void AllSamePrice()
        {
            Contest contest = CreateTestContest();
            Contestant c0 = contest.AddContestant("1", "One", 2.11m);
            Contestant c1 = contest.AddContestant("2", "Two", 2.11m);
            Contestant c2 = contest.AddContestant("3", "Three", 2.11m);

            List<Contestant> expected = new List<Contestant>() { c0, c1, c2 };

            var actual = contest.GetContestantsWithPriceAsc();

            Assert.Equal(expected, actual);

        }

        //unsorted
        [Fact]
        public void Unsorted()
        {
            Contest contest = CreateTestContest();
            Contestant c2 = contest.AddContestant("3", "Three", 8m);
            Contestant c0 = contest.AddContestant("1", "One", 5.7m);
            Contestant c3 = contest.AddContestant("4", "Four", 9.99m);
            Contestant c1 = contest.AddContestant("2", "Two", 7.6m);
            

            List<Contestant> expected = new List<Contestant>() { c0, c1, c2, c3 };

            var actual = contest.GetContestantsWithPriceAsc();

            Assert.Equal(expected, actual);

        }

        #endregion


        //parsing tests should be added here, could not add within 2 hours

        #region Utility
        private Contest CreateTestContest()
        {
            return new Contest("TestContestId", "TestContestName");
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Classes.Parser.Json
{
    public class Participant
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Tags { get; set; }
    }
}

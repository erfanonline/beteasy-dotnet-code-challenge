﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Classes.Parser.Json
{
    public class Selection
    {
        public string Id { get; set; }
        public decimal Price { get; set; }
        public Dictionary<string, string> Tags { get; set; }
    }
}

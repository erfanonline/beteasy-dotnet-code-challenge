﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Classes.Parser.Json
{
    public class Market
    {
        public string Id { get; set; }
        public List<Selection> Selections { get; set; }
        public Dictionary<string, string> Tags { get; set; }
    }
}

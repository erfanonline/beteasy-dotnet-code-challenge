﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Classes.Parser.Json
{
    public class RawData
    {
        public string FixtureName { get; set; }
        public string Id { get; set; }
        public Dictionary<string, string> Tags { get; set; }
        public List<Market> Markets { get; set; }
        public List<Participant> Participants { get; set; }
    }
}

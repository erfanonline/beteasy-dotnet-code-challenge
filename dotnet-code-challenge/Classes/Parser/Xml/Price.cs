﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace dotnet_code_challenge.Classes.Parser.Xml
{
    public class Price
    {
        [XmlArrayItem(ElementName = "horse")]
        [XmlArray(ElementName = "horses")]
        public HorsePrice[] HorsePrices { get; set; }
    }
}

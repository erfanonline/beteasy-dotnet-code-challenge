﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace dotnet_code_challenge.Classes.Parser.Xml
{
    [XmlRoot(ElementName = "meeting", Namespace = "")]
    public class RawMeeting
    {
        [XmlElement]
        public string date { get; set; }

        [XmlElement]
        public string MeetingType { get; set; }

        [XmlElement]
        public int Meetingid { get; set; }

        [XmlElement(ElementName = "track")]
        public Track Track { get; set; }

        [XmlArrayItem(ElementName = "race")]
        [XmlArray(ElementName = "races")]
        public Race[] Races { get; set; }
    }

    
}

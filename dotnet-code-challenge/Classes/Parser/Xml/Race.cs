﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace dotnet_code_challenge.Classes.Parser.Xml
{
    public class Race
    {
        [XmlAttribute(AttributeName = "id")]
        public int Id { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlArrayItem(ElementName = "horse")]
        [XmlArray(ElementName = "horses")]
        public Horse[] Horses { get; set; }

        [XmlArrayItem(ElementName = "price")]
        [XmlArray(ElementName = "prices")]
        public Price[] Prices { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace dotnet_code_challenge.Classes.Parser.Xml
{
    public class Track
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace dotnet_code_challenge.Classes.Parser.Xml
{
    public class HorsePrice
    {
        [XmlAttribute(AttributeName = "number")]
        public int Number { get; set; }

        [XmlAttribute(AttributeName = "Price")]
        public decimal Price { get; set; }

    }
}

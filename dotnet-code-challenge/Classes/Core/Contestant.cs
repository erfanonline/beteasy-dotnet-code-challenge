﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Classes.Core
{
    public class Contestant
    {
        public Contestant(string id, string name, decimal price)
        {
            Id = id;
            Name = name;
            Price = price;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        public string NameAndPrice
        {
            get
            {
                return string.Format("{0} (${1})", Name, Price.ToString("0.00"));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_code_challenge.Classes.Core
{
    public class Fixture
    {
        public Fixture(string id, string name)
        {
            Id = id;
            Name = name;
            Contests = new List<Contest>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public List<Contest> Contests { get; set; }

        public string GetContestantsWithPriceAscAsString()
        {
            StringBuilder outputBuilder = new StringBuilder();
            outputBuilder.AppendFormat("** Fixture:{0} ({1})\n", Name, Id);
            if (Contests != null)
            {
                foreach (Contest race in Contests)
                {
                    outputBuilder.Append(race.GetContestantsWithPriceAscAsString());
                }
            }
            outputBuilder.AppendFormat("**\n\n\n");
            return outputBuilder.ToString();
        }
    }
}

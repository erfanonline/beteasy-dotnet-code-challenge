﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dotnet_code_challenge.Classes.Core
{
    public class Contest
    {
        public Contest(string id, string name)
        {
            Id = id;
            Name = name;
            Contestants = new List<Contestant>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public List<Contestant> Contestants { get; set; }

        public Contestant AddContestant(string id, string name, decimal price)
        {
            Contestant contestant = new Contestant(id, name, price);
            Contestants.Add(contestant);
            return contestant;
        }

        public List<Contestant> GetContestantsWithPriceAsc()
        {
            if (Contestants != null && Contestants.Count > 1)
            {
                return Contestants.OrderBy(c => c.Price).ToList();
            }
            return Contestants;
        }

        public string GetContestantsWithPriceAscAsString()
        {
            List<Contestant> sortedContestants = GetContestantsWithPriceAsc();
            StringBuilder outputBuilder = new StringBuilder();
            outputBuilder.AppendFormat("# Race: {0} ({1})\n", Name, Id);
            outputBuilder.AppendFormat("Horse (Price) in ascending order of Prices:\n");
            if (sortedContestants != null)
            {
                foreach (Contestant horse in sortedContestants)
                {
                    outputBuilder.AppendFormat("{0}\n", horse.NameAndPrice);
                }
               
            }
            outputBuilder.Append("-----\n\n");
            return outputBuilder.ToString();
        }
    }
}

﻿using dotnet_code_challenge.Classes.Parser.Json;
using dotnet_code_challenge.Classes.Parser.Xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Serialization;
using System.Linq;

namespace dotnet_code_challenge.Classes.Core
{
    public class FixtureFactory
    {

        public static Fixture CreateFixture(FeedType feedType, string feedPath)
        {
            Fixture fixture = null;
            if (feedType == FeedType.Json)
            {
                try
                {
                    DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
                    settings.UseSimpleDictionaryFormat = true;
                    settings.DateTimeFormat = new System.Runtime.Serialization.DateTimeFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                    DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(RawFixture), settings);
                    object parsed = js.ReadObject(new FileStream(feedPath, FileMode.Open));
                    if (parsed == null)
                    {
                        throw new Exception("Serialization failed");
                    }
                    RawFixture rawFixture = (RawFixture)parsed;
                    fixture = new Fixture(rawFixture.FixtureId, rawFixture.RawData.FixtureName);
                    if (rawFixture.RawData.Markets != null)
                    {
                        foreach (Market race in rawFixture.RawData.Markets)
                        {
                            Contest contest = new Contest(race.Id, race.Id);
                            fixture.Contests.Add(contest);
                            if (race.Selections != null)
                            {
                                foreach (Selection horse in race.Selections)
                                {
                                    contest.AddContestant(horse.Id, horse.Tags["name"], horse.Price);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("Error parsing {0}", feedPath));
                    Console.WriteLine(ex.Message);
                }
            }
            else if (feedType == FeedType.Xml)
            {
                try
                {
                    XmlSerializer xs = new XmlSerializer(typeof(RawMeeting));
                    object parsed = xs.Deserialize(new FileStream(feedPath, FileMode.Open));
                    if (parsed == null)
                    {
                        throw new Exception("Serialization failed");
                    }

                    RawMeeting rawMeeting = (RawMeeting)parsed;
                    fixture = new Fixture(rawMeeting.Meetingid.ToString(), rawMeeting.Track.Name);
                    if (rawMeeting.Races != null)
                    {
                        foreach (Race race in rawMeeting.Races)
                        {
                            Contest contest = new Contest(race.Id.ToString(), race.Name);
                            fixture.Contests.Add(contest);
                            if (race.Horses != null)
                            {
                                foreach (Horse horse in race.Horses)
                                {
                                    contest.AddContestant(horse.Id.ToString(), horse.Name,
                                        race.Prices.First().HorsePrices.Single(p => p.Number == horse.Number).Price);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("Error parsing {0}", feedPath));
                    Console.WriteLine(ex.Message);
                }
            }
            return fixture;
        }

    }
}

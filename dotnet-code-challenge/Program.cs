﻿using dotnet_code_challenge.Classes.Core;
using System;

namespace dotnet_code_challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            Fixture xmlFixture = FixtureFactory.CreateFixture(FeedType.Xml, @".\FeedData\Caulfield_Race1.xml");
            Console.WriteLine(xmlFixture.GetContestantsWithPriceAscAsString());

            Fixture jsonFixture = FixtureFactory.CreateFixture(FeedType.Json, @".\FeedData\Wolferhampton_Race1.json");
            Console.WriteLine(jsonFixture.GetContestantsWithPriceAscAsString());
        }
    }
}
